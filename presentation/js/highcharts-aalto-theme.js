/**
 * Gray theme for Highcharts JS
 * @author Torstein Honsi
 */

Highcharts.theme = {
	colors: ["#009b3a", "#ff7900", "#0065bd", "#ed2939", "#fecb00", "#6639b7", "#4e9c6a", "#ffbb80", "#5e91bd", "#ed8c94", "#fce37e", "#8f79b8"],
	title: {
		style: {
			font: 'bold 24px Arial',
		},
		text: null,
	},
	subtitle: {
		style: {
			font: '24px Arial',
		}
	},
	xAxis: {
		labels: {
			style: {
				font: 'bold 24px Arial',
			}
		},
		title: {
			style: {
				font: 'bold 24px Arial',
				color: '#000000',
			},
			useHTML: true,
			text: null,
		},
		labels: {
			rotation: -45,
			align: 'center',
			useHTML: true,
			style: {
				paddingRight: '24px',
				paddingTop: '24px',
				font: '24px Arial',
			},
		},
	},
	yAxis: {
		labels: {
			style: {
				font: '24px Arial',
			}
		},
		title: {
			style: {
				font: 'bold 24px Arial',
				color: '#000000',
			},
			useHTML: true,
			text: null,
		}
	},
	legend: {
		enabled: false,
	},
	plotOptions: {
		series: {
			animation: false,
			groupPadding: 0,
			pointWidth: 60,
		},
		column: {
			colorByPoint: true,
		},
	},
};

// Apply the theme
var highchartsOptions = Highcharts.setOptions(Highcharts.theme);
